@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">CREAR PRODUCTO</div>

                <div class= "col text-right">
                 <a href="{{ route('list.productos') }}" class="btn btn-sm btn-success">Cancelar</a>
                </div>

                <div class="card-body">

                <form role="form" method="post" action="{{ route('guardar.productos')}}">
                    {{ csrf_field() }}
                    {{method_field('post')}}

                    <div class="row">
                        <div class="col-lg-4">
                        <label class="from-control-label" for="Nombre">Nombre del Producto</label>
                        <input type="text" class="from-control" name="Nombre">
                    </div>

                   <div class="col-lg-4">
                        <label class="from-control-label" for="Tipo">Tipo del Producto</label>
                        <input type="text" class="from-control" name="Tipo">
                        </div>

                        <div class="col-lg-4">
                        <label class="from-control-label" for="Estado">Estado del Producto</label>
                        <input type="number" class="from-control" name="Estado">
                        </div>

                        <div class="col-lg-4">
                        <label class="from-control-label" for="Precio">Precio del Producto</label>
                        <input type="number" class="from-control" name="Precio">
                        </div>

                    </div>     
                    <button type="submit" class="btn btn-success pull-rigth"> Guardar </button>
                    </form>
                
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
